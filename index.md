## [Data Visualization with Python]()

-----

## Follow Along

<a href="https://live-presentation.fsmk.org">
live-presentation.fsmk.org
</a>

-----

# Introduction

---

## Session

- Hands On <!-- .element: class="fragment " data-fragment-index="0" -->
- Data Visualization Basics <!-- .element: class="fragment " data-fragment-index="1" -->
- [BCS358D Lab](img/BCS358D.pdf) <!-- .element: class="fragment " data-fragment-index="2" -->
- Requires basic python <!-- .element: class="fragment " data-fragment-index="3" -->

-----

# [Data Science]()

---

## Data ?

---

Data - lat. plural for <!-- .element: class="fragment" data-fragment-index="0" --> <em>Datum</em> <!-- .element: class="fragment" data-fragment-index="1" --> 

i.e a fact or proposition <!-- .element: class="fragment" data-fragment-index="2" -->


Data = Collection of facts. <!-- .element: class="fragment" data-fragment-index="3" -->

---

## Science ?

---

<em>Techniques and frameworks to understand topics using experimental observations and mathematical inference.</em>

---

<div class="r-stack">

![](img/datascience1.png) <!-- .element: class="fragment current-visible" data-fragment-index="0" -->

![](img/datascience2.png) <!-- .element: class="fragment current-visible" data-fragment-index="1" -->

![](img/datascience3.png) <!-- .element: class="fragment current-visible" data-fragment-index="2" -->

</div>

---

If we define science [ broadly ] as

<br>

<em>"Applications of math pertaining to the world"</em>

---

Then, Data Science :

<em>"Applied statistics, informed by the study of algorithms and informatics, to process data in computerized environments"</em>

-----

# [Data Science Lifecycle]()

---

- Data Ingestion <!-- .element: class="fragment" data-fragment-index="0" --> 
- Data Storage <!-- .element: class="fragment" data-fragment-index="1" -->
- Data Processing <!-- .element: class="fragment" data-fragment-index="2" -->
- Data Analysis <!-- .element: class="fragment" data-fragment-index="3" -->
- Data Visualization <!-- .element: class="fragment" data-fragment-index="4" --> <!-- .element: style="color:#fa0" data-fragment-index="5" -->

-----

# [Data Visualization]()

---

## Why [Datavis]() ?

- Data is abstract <!-- .element: class="fragment" data-fragment-index="0" -->
- Visual Storytelling <!-- .element: class="fragment" data-fragment-index="1" -->
- Easier to Infer Trends <!-- .element: class="fragment" data-fragment-index="2" -->
- Helps Decision Making <!-- .element: class="fragment" data-fragment-index="3" -->

---

## [Tools]()

---

Data visualization requires 3 ingredients :

- Data <!-- .element: class="fragment" data-fragment-index="0" -->
- Data processing framework/platform <!-- .element: class="fragment" data-fragment-index="1" -->
- Visualisation tools <!-- .element: class="fragment" data-fragment-index="2" --> 

---

## Data

[ Shared Folder ](192.168.3.133)

---

## Data Processing Platforms

Tools and languages that can work together to process data.

[Examples]() : <!-- .element: class="fragment" data-fragment-index="0" -->

- Python - NumPy, Pandas <!-- .element: class="fragment" data-fragment-index="1" -->
- R - RStudio <!-- .element: class="fragment" data-fragment-index="2" -->
- Julia <!-- .element: class="fragment" data-fragment-index="3" -->

---

## Data Visualisation Tools

Frameworks, libraries and services that integrate with Data Processing Platforms

[Examples]() : <!-- .element: class="fragment" data-fragment-index="0" -->

- Python - Matplotlib, Seaborn, Bokeh, Plotly, Altair, Holoviz, Vizpy, Napari <!-- .element: class="fragment" data-fragment-index="1" -->
- JS - p5.js, d3.js, Observable <!-- .element: class="fragment" data-fragment-index="3" -->

-----

## DataVis : How to Choose a Chart

[Resource for Chart Decision](https://extremepresentation.typepad.com/files/choosing-a-good-chart-09.pdf)

-----

## Anaconda


- <span>Anaconda is a <em>python distribution, runtime and package manager </em></span> <!-- .element: class="fragment" data-fragment-index="0" -->
- Prepackaged libraries, frameworks and other python packages <!-- .element: class="fragment" data-fragment-index="1" -->
- IDE for data science <!-- .element: class="fragment" data-fragment-index="2" -->

---

- [Website](https://www.anaconda.com/)
- [Documentation](https://docs.anaconda.com/)

---

## Verify Installation

#### Linux 

Run 'conda' in a terminal to verify Anaconda's installation.

-----

## NumPy

Python Framework for Numerical Computing

- N-Dimensional Arrays <!-- .element: class="fragment" data-fragment-index="0" -->
- High Performance <!-- .element: class="fragment" data-fragment-index="1" -->
- Interoperable <!-- .element: class="fragment" data-fragment-index="2" -->

---

## Where is NumPy used ?

- BioInformatics
- Quantum Computing
- Signal + Image Processing
- AI/ML
- Astronomy & Cosmology
- Networking

---

- [Website](https://numpy.org/)
- [Documentation](https://numpy.org/doc/stable/)

---

## NumPy Exercises

- [Notebook](https://gitlab.com/ravish0007/numpy-pandas-fsmk/-/raw/master/NumPy_exercises.ipynb?ref_type=heads&inline=false)
- [Image](https://gitlab.com/ravish0007/numpy-pandas-fsmk/-/raw/master/amc-logo.jpg?ref_type=heads&inline=false)

-----

## Pandas

- Python framework for data analysis
- DataFrame creation
- Importing data from various sources - JSON, CSV, XLSX


---

- [Website](https://pandas.pydata.org/)
- [Documentation](https://pandas.pydata.org/docs/)

---

## Pandas Excercises

- [Notebook](https://gitlab.com/ravish0007/numpy-pandas-fsmk/-/raw/master/Pandas_Exercises.ipynb?ref_type=heads&inline=false)
- [Data](https://gitlab.com/ravish0007/numpy-pandas-fsmk/-/raw/master/automobile.csv?ref_type=heads&inline=false)

-----

# [Visualisation Examples]()

-----

## Matplotlib

- [Website](https://matplotlib.org/)
- [Documentation](https://matplotlib.org/stable/users/index)

---

## Lab Program 4a

[Notebook](https://gitlab.com/lab_manuals/bcs358d_data-visualization-with-python/-/raw/main/Programs/P04/P04A.ipynb?ref_type=heads&inline=false)

---

## Lab Program 4b

[Notebook](https://gitlab.com/lab_manuals/bcs358d_data-visualization-with-python/-/raw/main/Programs/P04/P04B.ipynb?ref_type=heads&inline=false)

---

## Lab Program 5a

[Notebook](https://gitlab.com/lab_manuals/bcs358d_data-visualization-with-python/-/raw/main/Programs/P05/P05A.ipynb?ref_type=heads&inline=false)

---

## Lab Program 5b

[Notebook](https://gitlab.com/lab_manuals/bcs358d_data-visualization-with-python/-/raw/main/Programs/P05/P05B.ipynb?ref_type=heads&inline=false)

---

## Lab Program 6

[Notebook](https://gitlab.com/lab_manuals/bcs358d_data-visualization-with-python/-/raw/main/Programs/P06/P06.ipynb?ref_type=heads&inline=false)


-----

## Seaborn

- [Website](https://seaborn.pydata.org/)
- [Documentation](https://seaborn.pydata.org/tutorial.html)

---

## Lab Program 7

[Notebook](https://gitlab.com/lab_manuals/bcs358d_data-visualization-with-python/-/raw/main/Programs/P07/P07.ipynb?ref_type=heads&inline=false)

-----

## Bokeh

- [Website](https://bokeh.org/)
- [Documentation](https://docs.bokeh.org/en/latest/)

---

## Lab Program 8

[Notebook](https://gitlab.com/lab_manuals/bcs358d_data-visualization-with-python/-/raw/main/Programs/P08/P08.ipynb?ref_type=heads&inline=false)

-----

## Plotly

- [Website](https://plotly.com/python/)

---

## Lab Program 9

[Notebook](https://gitlab.com/lab_manuals/bcs358d_data-visualization-with-python/-/raw/main/Programs/P09/P09.ipynb?ref_type=heads&inline=false)

---

## Lab Program 10a

- [Notebook](https://gitlab.com/lab_manuals/bcs358d_data-visualization-with-python/-/raw/main/Programs/P10/PART_A/Solution1/P10a.ipynb?ref_type=heads&inline=false)
- [Data](https://gitlab.com/lab_manuals/bcs358d_data-visualization-with-python/-/raw/main/Programs/P10/PART_A/Solution1/CUR_DLR_INR.csv?ref_type=heads&inline=false)

---

## Lab Program 10b

- [Notebook](https://gitlab.com/lab_manuals/bcs358d_data-visualization-with-python/-/raw/main/Programs/P10/PART_B/Solution2/P10b.ipynb?ref_type=heads&inline=false)
- [Data 1](https://gitlab.com/lab_manuals/bcs358d_data-visualization-with-python/-/raw/main/Programs/P10/PART_B/Solution2/india_census.csv?ref_type=heads&inline=false)
- [Data 2](https://gitlab.com/lab_manuals/bcs358d_data-visualization-with-python/-/raw/main/Programs/P10/PART_B/Solution2/states_india.geojson?ref_type=heads&inline=false)

-----

# Lab Manual

<br>

[VTU - FOSS Lab Manuals](https://gitlab.com/lab_manuals)

---

[Lab Manual - BCS358D](https://gitlab.com/lab_manuals/bcs358d_data-visualization-with-python)

-----

# Conclusion

-----

# Thank you !

